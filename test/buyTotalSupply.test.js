const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const LoaNTFPrivateSale = artifacts.require('LoaNTFPrivateSale');

contract('LoaNTFPrivateSale', function (accounts) {
    const [
        owner,
        address1,
        address2,
        address3,
        address4,
        address5,
        address6,
        address7,
        address8,
        address9,
    ] = accounts;

    const startDate = 1638230400
    const sixteen_decimal_value = '0000000000000000'

    before(async function () {
        this.loaNTFPrivateSale = await LoaNTFPrivateSale.new();
    });

    it('Should buy', async function () {
        await time.increaseTo(startDate)

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address1,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address1)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address2,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address2)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address3,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address3)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address4,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address4)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address5,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address5)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(6, {
                from: address6,
                value: '9600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address6)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(4, {
                from: address7,
                value: '6400' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address7)
            console.log(balance.toString())

            console.log('----')
        }

        {
            await this.loaNTFPrivateSale.buyImmortalSkin(1, {
                from: address8,
                value: '1600' + sixteen_decimal_value,
                gas: ('5000000')
            });

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(0)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(0, address8)
            console.log(balance.toString())

            console.log('----')
        }
    })
});
