const { BN, time, expectRevert} = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

const LoaNTFPrivateSale = artifacts.require('LoaNTFPrivateSale');

contract('LoaNTFPrivateSale', function (accounts) {
    const [
        owner,
        nonWhitelistAddress,
    ] = accounts;

    const startDate = 1638230400
    const sixteen_decimal_value = '0000000000000000'

    before(async function () {
        this.loaNTFPrivateSale = await LoaNTFPrivateSale.new();
    });

    // it('Should not buy if date has not yet started', async function () {
    //     await time.increaseTo(startDate - 1)
    //     await this.loaNTFPrivateSale.buyBetaCapture(1, { from: nonWhitelistAddress, value: '5' + sixteen_decimal_value, gas: ('5000000') });
    // })

    // it('Should buy', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFPrivateSale.buyBetaCapture(1, { from: nonWhitelistAddress, value: '5' + sixteen_decimal_value, gas: ('5000000') });
    // })

    // it('Should not buy if qty > supply', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFPrivateSale.buyBetaCapture(80001, { from: nonWhitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })

    // it('Should not buy if deposited amount is not the multiplier of quantity', async function () {
    //     await time.increaseTo(startDate)
    //     await this.loaNTFPrivateSale.buyBetaCapture(2, { from: nonWhitelistAddress, value: '9' + sixteen_decimal_value, gas: ('5000000') });
    // })

    // it('Should not buy if date has ended', async function () {
    //     await time.increaseTo(startDate + 86400 + 1)
    //     await this.loaNTFPrivateSale.buyBetaCapture(1, { from: nonWhitelistAddress, value: '1600' + sixteen_decimal_value, gas: ('5000000') });
    // })

    it('Should buy', async function () {
        {
            await time.increaseTo(startDate)
            await this.loaNTFPrivateSale.buyBetaCapture(3, {
                from: nonWhitelistAddress,
                value: '15' + sixteen_decimal_value,
                gas: ('5000000')
            });
            let amount = await this.loaNTFPrivateSale._itemOwned(3, nonWhitelistAddress)
            console.log(amount.toString())

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(3)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(3, nonWhitelistAddress)
            console.log(balance.toString())
        }

        {
            await time.increaseTo(startDate + 600)
            await this.loaNTFPrivateSale.buyBetaCapture(4, {
                from: nonWhitelistAddress,
                value: '20' + sixteen_decimal_value,
                gas: ('5000000')
            });
            let amount = await this.loaNTFPrivateSale._itemOwned(3, nonWhitelistAddress)
            console.log(amount.toString())

            let totalSupply = await this.loaNTFPrivateSale._itemTotalSupply(3)
            console.log(totalSupply.toString())

            let balance = await this.loaNTFPrivateSale._itemOwned(3, nonWhitelistAddress)
            console.log(balance.toString())
        }
    })
});
